# -*- coding: utf-8 -*-
"""The entry point for the commandline-interface for *frkl.project*."""
import asyncclick as click
from rich import print as rich_print
from rich.syntax import Syntax

from frkl.project.interfaces.cli.command_classes import FrklProjectBase

# try:
#     import uvloop
#     uvloop.install()
# except Exception:
#     pass


click.anyio_backend = "asyncio"


pass_base = click.make_pass_decorator(FrklProjectBase, ensure=True)

cli = FrklProjectBase()


@cli.group()
@pass_base
async def metadata(base: FrklProjectBase):
    """Display different kinds of metadata for a project."""


@metadata.command("print")
@pass_base
async def print_metadata(base: FrklProjectBase):
    json_str = base.project.json(indent=2)
    rich_print(Syntax(json_str, "json", background_color="default"))


#
# @cli.command()
# @click.argument("config", nargs=1, required=False)
# @click.pass_context
# async def create(ctx, config: str):
#     """Create project structure from template."""
#
#     template_manager: ProjectTemplateManager = ctx.obj["template_manager"]
#     # print(template_manager.project_cls.ConfigSchema.schema())
#
#     typistry = template_manager._tyngspace
#
#     if not config:
#         _config = await typistry.create_tyng_config_from_source(
#             PROJECT_ID_NAMESPACE, "tui"
#         )
#     else:
#         _config = await get_content_async(config)
#
#     # print('---')
#     # import pp
#     # pp(config)
#
#     project = await template_manager.create_project_obj(_config)
#     path = Path(".")
#     project.execute_command("create_python_project", parent_path=path)


# @metadata.command("print")
# @click.option(
#     "--main-module",
#     "-m",
#     required=False,
#     help="the name of the project main module (guessed if not provided)",
# )
# @click.option(
#     "--output", "-o", required=False, help="write output to a file, instead of stdout"
# )
# @click.pass_context
# def print_metadata(ctx, main_module: str, output: Optional[str] = None):
#     """Print project metadata."""
#
#     if not main_module:
#         main_module = guess_main_module()
#
#     md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)
#
#     md_json = json.dumps(
#         md_obj.to_dict(), sort_keys=True, indent=2, separators=(",", ": ")
#     )
#
#     if output is None:
#         print(md_json)
#     else:
#         output_file = Path(output)
#         output_file.parent.mkdir(parents=True, exist_ok=True)
#
#         print(f"Writing metadata to: {output_file.as_posix()}")
#         output_file.write_text(md_json)
#
#
# @metadata.command()
# @click.option(
#     "--main-module",
#     "-m",
#     required=False,
#     help="the name of the project main module (guessed if not provided)",
# )
# def print_runtime_info(main_module: str):
#     """Print runtime metadata."""
#
#     if not main_module:
#         main_module = guess_main_module()
#
#     md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)
#
#     md_json = json.dumps(
#         md_obj.runtime_details, sort_keys=True, indent=2, separators=(",", ": ")
#     )
#     print(md_json)
#
#
# @metadata.command()
# @click.option(
#     "--main-module",
#     "-m",
#     required=False,
#     help="the name of the project main module (guessed if not provided)",
# )
# @click.option(
#     "--output", "-o", required=False, help="write output to a folder, instead of stdout"
# )
# @click.pass_context
# async def print_pyinstaller_data(ctx, main_module: str, output: Optional[str] = None):
#     """Print PyInstaller-related metadata."""
#
#     if not main_module:
#         main_module = guess_main_module()
#
#     # md_obj: ProjectMetadata = ProjectMetadata(project_main_module=main_module)
#
#     template_manager: ProjectTemplateManager = ctx.obj["template_manager"]
#     project_metadata = find_project_metadata(main_module)
#     project = await template_manager.create_project_obj(project_metadata)
#
#     renderer = PyinstallerConfig(project)
#
#     if not output:
#         analysis_args = renderer.create_analysis_args(None)
#
#         md_json = json.dumps(
#             analysis_args, sort_keys=True, indent=2, separators=(",", ": ")
#         )
#         print(md_json)
#     else:
#         output_path = Path(output)
#         args_file = output_path / "pyinstaller_args.json"
#         output_path.mkdir(parents=True, exist_ok=True)
#
#         analysis_args = renderer.create_analysis_args(output_path)
#
#         md_json = json.dumps(
#             analysis_args, sort_keys=True, indent=2, separators=(",", ": ")
#         )
#
#         print(f"Writing pyinstaller config to folder: {output_path.as_posix()}")
#         args_file.write_text(md_json)


if __name__ == "__main__":
    cli()
