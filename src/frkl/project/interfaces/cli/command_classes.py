# -*- coding: utf-8 -*-
import asyncclick as click
import logging
import os.path
import slugify
import typing
from anyio import create_task_group
from asyncclick import (
    Argument,
    Command,
    Group,
    MultiCommand,
    Option,
    get_current_context,
)
from pathlib import Path
from pydantic import BaseModel
from typing import Any, Dict, Iterable, Mapping, Optional, Type, Union

from frkl.common.async_utils import _run_in_thread
from frkl.common.doc import Doc
from frkl.common.exceptions import FrklException
from frkl.project.template import Project, ProjectTemplateManager, ProjectType
from frkl.project.template.project_types import ProjectCommand
from frkl.typistry.tyng_space import TyngSpace

log = logging.getLogger("frkl.project")


class FrklProjectBase(Group):
    def __init__(
        self,
        main_module: Optional[str] = None,
        project_path: Union[None, Path, str] = None,
        tyngspace: Optional[TyngSpace] = None,
    ):

        if project_path is None:
            pass
        elif isinstance(project_path, str):
            project_path = Path(os.path.expanduser(project_path))
        elif not isinstance(project_path, Path):
            raise TypeError(project_path)

        self._project_path: typing.Optional[Path] = project_path
        self._main_module: Optional[str] = main_module
        self._tyngspace: Optional[TyngSpace] = tyngspace
        self._template_manager: Optional[ProjectTemplateManager] = None
        self._project: Optional[Project] = None

        params = None
        if self._project_path is None:
            option = Option(
                param_decls=["--project-path", "-p"],
                help="The project path. Defaults to current directory.",
                type=click.Path(
                    exists=False,
                    file_okay=False,
                    dir_okay=True,
                    writable=True,
                    readable=True,
                    resolve_path=True,
                ),
                required=False,
            )
            params = [option]

        super().__init__(callback=self.entry_point, params=params)
        self.add_command(CreateProjectCommand(base_command=self), "create")
        self.add_command(DoMultiCommand(base_command=self), "do")

    async def entry_point(self, **kwargs):

        ctx = get_current_context()
        ctx.obj = self

        project_path = kwargs.get("project_path", None)
        if project_path:
            self._project_path = Path(os.path.expanduser(kwargs["project_path"]))
        else:
            self._project_path = Path.cwd()

        if self._project_path.exists():
            try:
                self.project = await self._create_project_obj()
            except Exception as e:
                print(e)
                import traceback

                traceback.print_exc()

    @property
    def project_path(self):
        return self._project_path

    @property
    def tyngspace(self) -> TyngSpace:
        if self._tyngspace is None:
            self._tyngspace = TyngSpace.instance()
        return self._tyngspace

    @property
    def template_manager(self) -> ProjectTemplateManager:
        if self._template_manager is None:
            self._template_manager = ProjectTemplateManager(tyngspace=self.tyngspace)
        return self._template_manager

    @property
    def project(self) -> Project:
        if self._project is not None:
            return self._project

        raise FrklException(
            msg="Project not set yet.",
            reason=f"No valid project found for path: {self.project_path}.",
        )

    @project.setter
    def project(self, project: Project):

        if self._project:
            raise FrklException(msg="Can't set project.", reason="Project already set.")

        self._project = project

    async def _create_project_obj(self) -> Project:

        if not self.project_path.exists():
            raise FrklException(
                msg="Can't open project.",
                reason=f"Project path does not exist: {self.project_path.as_posix()}",
            )

        project_types = self.tyngspace.typistry.get_subclasses(ProjectType)
        if len(project_types) > 1:

            possible_configs: Dict[str, Optional[Mapping[str, Any]]] = {}
            async with create_task_group() as tg:

                async def find_project_config(name, cls):
                    config = await cls.get_project_config(self.project_path)
                    if config:
                        possible_configs[name] = config

                for name, project_type_cls in project_types.items():
                    tg.start_soon(find_project_config, name, project_type_cls)

            if len(possible_configs) == 0:
                raise FrklException(
                    msg=f"Can't find project metadata for path: {self.project_path}."
                )
            elif len(possible_configs) > 1:
                raise FrklException(
                    msg=f"Can't assemble project metadata for path: {self.project_path}",
                    reason=f"Multiple possible project types found: {', '.join(possible_configs.keys())}",
                )
            project_config = next(iter(possible_configs.keys()))

        else:

            project_type = next(iter(project_types.values()))
            project_config = await project_type.get_project_config(self.project_path)

        project = await self.template_manager.create_project_obj(config=project_config)
        return project


class CreateProjectCommand(Command):
    def __init__(self, base_command: FrklProjectBase):

        self._base: FrklProjectBase = base_command
        help_str = "Create a project from a template."
        params = [
            Argument(
                ["base_path"],
                type=click.Path(
                    exists=False,
                    file_okay=False,
                    dir_okay=True,
                    writable=True,
                    readable=True,
                    resolve_path=True,
                ),
                required=False,
            ),
            Option(
                param_decls=["--data-source", "-s"],
                help="The project information data source.",
                required=False,
                default="tui",
            ),
        ]
        super().__init__(
            short_help=help_str,
            name="create",
            callback=self.create_project,
            params=params,
        )

    async def create_project(
        self, base_path: Union[str, Path], data_source: str = "tui"
    ) -> Path:

        if not base_path:
            base_path = Path.cwd()
        elif isinstance(base_path, str):
            base_path = Path(os.path.expanduser(base_path))

        project_obj: Project = await self._base.template_manager.create_project_obj(
            config=None, project_id=None, data_source=data_source
        )
        project_path = await project_obj.project_type.create_project(
            base_path=base_path, project=project_obj
        )
        return project_path


class DoMultiCommand(MultiCommand):
    def __init__(self, base_command: FrklProjectBase):

        self._base: FrklProjectBase = base_command
        self._command_subclasses: Optional[Mapping[str, Type[ProjectCommand]]] = None
        help_str = """Execute project-type specific commands."""
        super().__init__(short_help=help_str)

    async def load_supported_commands(self):

        command_subclasses = self._base.tyngspace.typistry.register_subclasses(
            ProjectCommand, ignore_if_exists=True
        )

        if not command_subclasses:
            log.warning("No command subclasses found.")
            self._command_subclasses = {}
            return

        if len(command_subclasses) > 1:

            supported_commands: Dict[str, Optional[Mapping[str, Any]]] = {}
            async with create_task_group() as tg:

                async def check_project(name, cls: ProjectCommand):
                    is_supported = cls.is_supported(self._base.project)
                    if is_supported:
                        supported_commands[name] = cls

                for name, command_cls in command_subclasses.items():
                    tg.start_soon(check_project, name, command_cls)

        else:

            cls = next(iter(command_subclasses.values()))
            supported_commands = {}
            if cls.is_supported(self._base.project):
                supported_commands[next(iter(command_subclasses.keys()))] = cls

        if len(supported_commands) == 0:
            log.warning("No supported commands found for current project type.")
            self._command_subclasses = {}
            return

        result = {}
        for alias, cls in supported_commands.items():
            if hasattr(cls, "_cmd_name"):
                alias = cls._cmd_name  # type: ignore
            else:
                alias = slugify.slugify(alias, separator="-")
            if alias in result.keys():
                raise FrklException(
                    msg="Can't create list of subcommands.",
                    reason=f"Duplicate subcommand name: {alias}",
                )
            result[alias] = cls

        return result

    @property
    def command_subclasses(self) -> Mapping[str, Type]:

        if self._command_subclasses is not None:
            return self._command_subclasses

        # TODO: make this smarter
        command_subclasses = _run_in_thread(self.load_supported_commands)
        self._command_subclasses = command_subclasses
        return self._command_subclasses

    def list_commands(self, ctx):
        return sorted(self.command_subclasses.keys())

    def get_command(self, ctx, cmd_name):
        base_cls = self.command_subclasses[cmd_name]
        cmd = ProjectCommandCommand(
            base_command=self._base, project_command_cls=base_cls, name=cmd_name
        )
        return cmd


class ProjectCommandCommand(Command):
    def __init__(
        self,
        base_command: FrklProjectBase,
        project_command_cls: Type[ProjectCommand],
        name: str,
    ):

        self._base: FrklProjectBase = base_command
        self._command_cls: Type[ProjectCommand] = project_command_cls

        self._doc: Doc = Doc.from_docstring(self._command_cls)
        kwargs: Dict[str, Any] = {"name": name}
        kwargs["params"] = self._create_parameters()
        kwargs["callback"] = self.execute_command
        kwargs["short_help"] = self._doc.get_short_help()

        super().__init__(**kwargs)

    def _create_parameters(self) -> Iterable[Union[Argument, Option]]:

        _model_cls: Type[BaseModel] = self._command_cls.ConfigSchema

        if "__root__" in _model_cls.__fields__:
            raise NotImplementedError()
        result = []
        for field_name, field in _model_cls.__fields__.items():
            desc = field.field_info.description
            if not desc:
                desc = "-- n/a --"
            option = Option(
                param_decls=[f"--{field_name}"],
                help=desc,
                required=field.required,
                default=field.default,
            )
            result.append(option)

        return result

    async def execute_command(self, **kwargs) -> None:

        cmd: ProjectCommand = self._command_cls(config=kwargs)
        cmd.process(self._base.project)
