# -*- coding: utf-8 -*-
import os
from typing import Iterable, Mapping, Optional, Union

from frkl.common.exceptions import FrklException
from frkl.common.formats.auto import (
    create_object_from_list_of_strings,
    get_content_async,
)
from frkl.project.template.hosting_services import HostingService
from frkl.project.template.project import Project, ProjectDetails
from frkl.project.template.project_types import ProjectType
from frkl.typistry.tyng_space import TyngSpace

PROJECT_ID_NAMESPACE = "frkl.project"


class ProjectTemplateManager(object):
    def __init__(self, tyngspace: Optional[TyngSpace] = None):

        self._tyngspace: TyngSpace = TyngSpace.instance_if_not(tyngspace)
        self._tyngspace.add_preload_modules(
            "frkl.project.template.project_types.*",
            "frkl.project.template.hosting_services.*",
            "frkl.typistry.data_source.tui",
        )

        self._tyngspace.register_tyng_type(
            PROJECT_ID_NAMESPACE,
            ProjectDetails,
            ProjectType,
            HostingService,
            _base_cls=Project,
            _class_name="FrklProject",
        )
        self._default_data_source: str = "tui"

    async def create_project_obj(
        self,
        config: Union[None, str, Mapping] = None,
        project_id: Optional[str] = None,
        data_source: Optional[str] = None,
    ) -> "Project":

        if not data_source:
            data_source = "__no_datasource__"
        elif data_source == "default":
            data_source = self._default_data_source

        if project_id and not isinstance(project_id, str):
            raise TypeError("Project id needs to be string.")

        if not config:
            if data_source == "__no_datasource__":
                raise FrklException(
                    msg="Can't create project.", reason="No configuration provided."
                )
            else:
                _config = await self._tyngspace.create_tyng_config_from_source(
                    PROJECT_ID_NAMESPACE, data_source
                )
        elif isinstance(config, str):
            if os.path.isfile(os.path.expanduser(config)):
                _config = await get_content_async(config)
            else:
                raise FrklException(
                    f"Can't load project config from path: {config}",
                    reason="Path does not exist or is not a file.",
                )
        elif isinstance(config, Mapping):
            _config = config
        elif isinstance(config, Iterable):
            _config = create_object_from_list_of_strings(config)
        else:
            raise TypeError(f"Invalid config type: {type(config)}")

        tyng_obj = self._tyngspace.create_tyng(type_name=PROJECT_ID_NAMESPACE, config=_config)  # type: ignore

        if not project_id:
            project_id = tyng_obj.config.project_details.project_slug

        full_project_id = f"{PROJECT_ID_NAMESPACE}.{project_id}"

        project_obj = self._tyngspace.register_tyng(
            tyng_id=full_project_id, tyng=tyng_obj
        )

        return project_obj

    @property
    def supported_project_types(self) -> Iterable[str]:
        return self._tyngspace.get_tyng_subclasses("project_type").keys()

    @property
    def supported_hosting_services(self) -> Iterable[str]:
        return self._tyngspace.get_tyng_subclasses("hosting_service").keys()
