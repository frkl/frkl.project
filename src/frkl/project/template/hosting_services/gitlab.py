# -*- coding: utf-8 -*-
import asyncstdlib
import gidgetlab.httpx
import httpx
import os
import typing
from anyio import create_task_group
from asyncstdlib import lru_cache
from contextlib import asynccontextmanager
from pydantic import PrivateAttr
from typing import ClassVar

from frkl.common.exceptions import FrklException
from frkl.project.template import ProjectDetails
from frkl.project.template.hosting_services import HostingService

DEFAULT_GITLAB_SERVER = "https://gitlab.com"


class GitLab(HostingService):
    _type_alias: ClassVar = "gitlab"

    _client: typing.Optional[gidgetlab.httpx.GitLabAPI] = PrivateAttr(default=None)

    def login(self, *args, **kwargs):
        pass

    @asyncstdlib.cached_property
    async def all_namespaces(self) -> typing.Mapping[str, typing.Dict[str, typing.Any]]:

        async with self.gitlab_api() as api:
            data = api.getiter("/namespaces")

            result = {}
            async for ns in data:
                result[ns["full_path"]] = ns

        return result

    @asyncstdlib.cached_property
    async def all_user_projects(
        self,
    ) -> typing.Mapping[str, typing.Dict[str, typing.Any]]:

        result = {}
        async with self.gitlab_api() as api:
            data = api.getiter("/projects")
            async for project in data:
                result[project["path_with_namespace"]] = project

        return result

    async def get_all_group_projects(
        self,
    ) -> typing.Dict[str, typing.Dict[str, typing.Any]]:

        result = {}

        async def _get_projects(ns_id: int):
            projects = await self.get_group_projects(ns_id)
            result.update(projects)

        all_ns = await self.all_namespaces
        async with create_task_group() as tg:
            for ns_id in (ns["id"] for ns in all_ns.values()):
                tg.start_soon(_get_projects, ns_id)

        return result

    async def get_all_projects(self) -> typing.Dict[str, typing.Dict[str, typing.Any]]:

        result = {}

        async def _get_group_projects():
            projects = await self.get_all_group_projects()
            result.update(projects)

        async def _get_user_projects():
            projects = await self.all_user_projects
            result.update(projects)

        async with create_task_group() as tg:
            tg.start_soon(_get_group_projects)
            tg.start_soon(_get_user_projects)

        return result

    @lru_cache()
    async def get_group_projects(
        self, group_name_or_id: typing.Union[int, str]
    ) -> typing.Mapping[str, typing.Dict[str, typing.Any]]:

        if isinstance(group_name_or_id, str):
            group_name_or_id = await self.get_namespace_id(group_name_or_id)  # type: ignore
            assert group_name_or_id is not None

        result = {}
        async with self.gitlab_api() as api:
            data = api.getiter(f"/groups/{group_name_or_id}/projects")
            async for project in data:
                result[project["path_with_namespace"]] = project

        return result

    @asynccontextmanager
    async def gitlab_api(self) -> gidgetlab.httpx.GitLabAPI:

        c = httpx.AsyncClient()
        try:
            pw = os.environ["GITLAB_TOKEN"]
            assert pw
            api = gidgetlab.httpx.GitLabAPI(c, self.login_user, access_token=pw)
            yield api
        finally:
            await c.aclose()

    async def get_namespace_id(self, user_or_group_name: str) -> typing.Optional[int]:

        ns = await self.all_namespaces
        ns_id: typing.Optional[int] = None
        for name, details in ns.items():
            if name == user_or_group_name:
                ns_id = details["id"]
                break
        return ns_id

    async def create_project_on_service(
        self, project_details: ProjectDetails, allow_exist: bool = False
    ) -> typing.Dict[str, typing.Any]:

        if self.config.public:  # type: ignore
            visibility = "public"
        else:
            visibility = "private"
        params = {"name": project_details.project_name, "visibility": visibility}

        if self.login_user == self.project_user:
            all_projects = await self.all_user_projects
        else:
            all_projects = await self.get_group_projects(self.project_user)
            ns_id = await self.get_namespace_id(self.project_user)
            params["namespace_id"] = ns_id

        full_path = f"{self.project_user}/{project_details.project_name}"
        if full_path in all_projects.keys():
            if not allow_exist:
                raise FrklException(
                    msg=f"Can't create project '{project_details.project_name}' for github user/group '{self.project_user}'.",
                    reason="Project with that name already exists.",
                )
            else:
                return all_projects[full_path]

        async with self.gitlab_api() as api:
            data = await api.post("/projects", data=params)

        if self.login_user == self.project_user:
            del self.all_user_projects
        else:
            self.get_group_projects.cache_clear()

        return data
