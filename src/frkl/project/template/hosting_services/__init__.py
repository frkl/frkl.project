# -*- coding: utf-8 -*-
import abc
import typing
from pydantic import BaseModel, Field, constr, validator

from frkl.typistry import Tyng

if typing.TYPE_CHECKING:
    from frkl.project.template import Project, ProjectDetails


class HostingServiceConfig(BaseModel):

    project_user: str = Field(
        description="The username that owns the project.",
        default_desc="The login user.",
    )
    login_user: constr(min_length=1) = Field(  # type: ignore
        description="The login username for the project hosting service."
    )
    public: bool = Field(
        description="Whether the project is publicly available on the service, or not.",
        default=True,
    )

    @validator("login_user")
    def default_project_user(cls, v, *, values, **kwargs):
        return v or values["login_user"]


class HostingService(Tyng, abc.ABC):
    class ConfigSchema(HostingServiceConfig):
        pass

    def get_repo_url(self, project: "Project"):
        return f"https://{self.type_alias}.com/{self.config.project_user}/{project.project_details.project_name}"  # type:ignore

    @abc.abstractmethod
    def login(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    async def create_project_on_service(self, project_details: "ProjectDetails"):
        pass

    # class HostingServiceConfig(BaseModel):
    #     class Config:
    #         extra = Extra.forbid
    #
    #     project_user: str = Field(description="The name of the user that owns the project.")
    #     project_name: typing.Optional[str] = Field(
    #         description="The name under which the project is hosted (if not provided, the default 'project_name' is used."
    #     )
