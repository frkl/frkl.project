# -*- coding: utf-8 -*-
import abc
import typing
from pathlib import Path
from pydantic import BaseModel, Field

from frkl.typistry import Tyng, TyngConf

if typing.TYPE_CHECKING:
    from frkl.project.template import Project


class TemplateLocation(BaseModel):
    template_url: str = Field(description="The git url to the template.")
    version: typing.Optional[str] = Field(
        description="The branch to check out, use the default branch if not specified.",
        default=None,
    )
    sub_directory: typing.Optional[str] = Field(
        description="The (optional) name of the sub directory of the template, in case of a multi-template git repo.",
        default=None,
    )


class ProjectType(Tyng, abc.ABC):
    @classmethod
    @abc.abstractmethod
    async def get_project_config(
        cls, project_folder: Path
    ) -> typing.Optional[typing.Mapping[str, typing.Any]]:
        """Return the configuration of a project if it exists in this folder.

        Arguments:
            project_folder: the base project folder

        Returns:
            a project configuration dictionary if a project exists, otherwise None
        """
        return None

    @classmethod
    @abc.abstractmethod
    async def create_project(cls, base_path: Path, project: "Project") -> Path:
        pass

    @abc.abstractmethod
    def get_template_location(self, version: str = "default") -> TemplateLocation:

        pass

    @abc.abstractmethod
    def create_project_type_properties(
        self, project: "Project"
    ) -> typing.Mapping[str, typing.Any]:
        pass

    def get_supported_commands(self):
        pass


class ProjectCommand(Tyng, abc.ABC):
    class ConfigSchema(TyngConf):
        pass

    def __init__(self, *args, **kwargs):
        Tyng.__init__(self, *args, **kwargs)

    @classmethod
    @abc.abstractmethod
    def is_supported(self, project: "Project") -> bool:
        pass

    @abc.abstractmethod
    def process(self, project: "Project"):
        pass
