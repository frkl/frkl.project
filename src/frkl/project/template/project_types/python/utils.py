# -*- coding: utf-8 -*-
import json
import logging
import os
import sys
import typing
from pathlib import Path
from typing import Any, Dict, Iterable, Mapping, Optional

from frkl.project.defaults import PROJECT_META_DEFAULT_IGNORE_MODULES
from frkl.project.meta.core import (
    PythonProjectMetadata,
    retrieve_metadata_for_project_main_module,
)


def find_frkl_projects(
    exclude_main_modules: Iterable[str],
) -> Mapping[str, PythonProjectMetadata]:
    """Return metadata objects for other included (and supported) dependency packages."""

    if hasattr(sys, "frozen"):
        raise Exception(
            "Querying dependency projects not supported for frozen applications."
        )

    modules = discover_installed_modules()
    _other_metadata_projects = {}
    for m in modules.keys():
        if m in exclude_main_modules:
            continue

        p = PythonProjectMetadata(project_main_module=m)
        _other_metadata_projects[p.main_module] = p

    log.debug(
        f"(main) dependencies for main module '{exclude_main_modules}': {_other_metadata_projects}"
    )
    return _other_metadata_projects


# def other_frkl_project_versions(self) -> Mapping[str, str]:
#     """Retrurn the versions of other included (and supported) dependency packages."""
#
#     if self._other_metadata_project_versions is None:
#         self._other_metadata_project_versions = {
#             p.main_module: p.version for p in self.other_frkl_projects.values()
#         }
#     return self._other_metadata_project_versions


log = logging.getLogger("frkl")


def find_unique_main_module_folder(base_dir: Path):

    single_folder = None
    for child in base_dir.iterdir():
        if child.name.endswith("egg-info"):
            continue
        if child.name.startswith(".") or child.name.startswith("_"):
            continue
        if not child.is_dir():
            continue
        if single_folder is not None:
            single_folder = None
            break
        single_folder = child

    if single_folder is None:
        return None

    frkl_module = single_folder / "_frkl" / "__init__.py"
    if frkl_module.exists():
        return single_folder

    return find_unique_main_module_folder(single_folder)


def guess_main_module(base_dir: typing.Optional[Path] = None):

    if base_dir is None:
        base_dir = Path(".")
    md_dir = base_dir / ".frkl"
    md_dir.mkdir(parents=True, exist_ok=True)
    md_file = md_dir / "project.json"
    if md_file.exists():

        with open(md_file) as f:
            data = json.load(f)
        main_module = data["main_module"]
    else:
        src_dir = base_dir / "src"
        folder = find_unique_main_module_folder(src_dir)
        if folder is None:
            print(
                "Can't guess project main module, please provide via the '--main-module' option."
            )
            sys.exit(1)

        rel = folder.relative_to(src_dir)
        main_module = str(rel).replace(os.path.sep, ".")

    return main_module


def discover_installed_modules(
    ignore_modules: Optional[Iterable[str]] = None,
    only_modules: Optional[Iterable[str]] = None,
) -> Dict[str, Mapping[str, Any]]:
    """Method that tries to find all other (relevant) packages/base-modules which are contained in this application.

    Args:
        ignore_modules: a list of modules to ignore (to speed up parsing, defaults to in-build list)
        only_modules: if specified, only modules contained in this list are used (to speed up parsing)

    Results:
        Set[ModuleType]: a set containing all relevant (base) modules that contain a '_frkl' sub-module.
    """

    import pkg_resources

    if ignore_modules is None:
        ignore_modules = PROJECT_META_DEFAULT_IGNORE_MODULES

    installed_packages = pkg_resources.working_set

    metadata_modules: Dict[str, Mapping[str, Any]] = {}
    for i in installed_packages:
        pkg_name = i.key

        if pkg_name in ignore_modules:
            continue

        if only_modules is not None:
            if pkg_name not in only_modules:
                continue

        log.debug(f"querying package: {i}")

        try:
            _pkg_name = pkg_name.replace("-", "_")
            if _pkg_name in metadata_modules:
                continue

            metadata = retrieve_metadata_for_project_main_module(_pkg_name)
            metadata_modules[_pkg_name] = metadata

        except (Exception):
            pass

    return metadata_modules
