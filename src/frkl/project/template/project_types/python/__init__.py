# -*- coding: utf-8 -*-
import cruft
import json
import os
import typing
from cookiecutter.generate import generate_files
from cruft._commands.utils.cookiecutter import (
    generate_cookiecutter_context,
    get_cookiecutter_repo,
    resolve_template_url,
)
from pathlib import Path
from pydantic import Field
from tempfile import TemporaryDirectory

from frkl.common.exceptions import FrklException
from frkl.project.meta.core import PythonProjectMetadata
from frkl.project.template import Project
from frkl.project.template.project_types import (
    ProjectCommand,
    ProjectType,
    TemplateLocation,
)
from frkl.typistry import TyngConf
from frkl.typistry.types.string_types import ValidModuleName


class PythonProjectType(ProjectType):
    """An opinionated Python project, using setuptools."""

    _type_alias = "python_project"

    class ConfigSchema(TyngConf):
        project_main_module: ValidModuleName = Field(
            description="The project main module."
        )
        exe_name: typing.Optional[str] = Field(
            description="The name of the executable (if applicable, leave empty if the project doesn't produce one).",
            default=None,
        )

    @property
    def project_main_module(self) -> str:
        return self.get_config("project_main_module")

    @property
    def exe_name(self) -> str:
        return self.get_config("exe_name")

    @classmethod
    async def get_project_config(
        cls, project_folder: Path
    ) -> typing.Optional[typing.Mapping[str, typing.Any]]:

        src_folder = project_folder / "src"
        if not src_folder.is_dir():
            return None

        project_files = src_folder.glob("*.project")
        files = []
        for pf in project_files:
            if pf.name.startswith(".") and pf.name.endswith(".project"):
                files.append(pf)

        if len(files) == 0:
            return None
        elif len(files) > 1:
            raise FrklException(
                f"Can't find unique project metadata file in '{project_folder.as_posix()}'.",
                reason=f"Found multiple metadata files: {files}",
            )

        md_file = files[0]
        content = md_file.read_text()
        config = json.loads(content)
        return config

    @classmethod
    async def create_project(cls, base_path: Path, project: Project) -> Path:

        # TODO: more details for copyright notice?
        # code adapted from https://github.com/cruft/cruft/blob/master/cruft/_commands/create.py, Copyright (c) 2020 Timothy Crosley, Sambhav Kothari

        if isinstance(base_path, str):
            base_path = Path(os.path.expanduser(base_path))

        template_loc = project.project_type.get_template_location()
        checkout = template_loc.version
        default_config = False
        config_file = None

        extra_context = project.project_type.create_project_type_properties(project)

        no_input = True
        overwrite_if_exists = False

        template_url = resolve_template_url(template_loc.template_url)
        with TemporaryDirectory() as cookiecutter_template_dir_str:
            cookiecutter_template_dir = Path(cookiecutter_template_dir_str)
            repo = get_cookiecutter_repo(
                template_url, cookiecutter_template_dir, checkout
            )
            last_commit = repo.head.object.hexsha

            if template_loc.sub_directory:
                cookiecutter_template_dir = (
                    cookiecutter_template_dir / template_loc.sub_directory
                )

            context = generate_cookiecutter_context(
                template_loc.template_url,
                cookiecutter_template_dir,
                config_file,
                default_config,
                extra_context,
                no_input,
            )

            project_dir = Path(
                generate_files(
                    repo_dir=cookiecutter_template_dir,
                    context=context,
                    overwrite_if_exists=overwrite_if_exists,
                    output_dir=base_path.as_posix(),
                )
            )

            # After generating the project - save the cruft state
            # into the cruft file.
            cruft_data = {
                "template": template_loc.template_url,
                "commit": last_commit,
                "checkout": checkout,
                "context": context,
                "directory": template_loc.sub_directory,
            }

            (project_dir / ".cruft.json").write_text(
                cruft._commands.utils.cruft.json_dumps(cruft_data)
            )

            return project_dir

    def get_template_location(self, version: str = "default") -> TemplateLocation:

        if version != "default":
            raise NotImplementedError()

        template_url = "https://gitlab.com/frkl/frkl.project.git"

        if os.environ.get("DEBUG", None) == "true":
            template_url = os.path.realpath(
                os.path.join(os.path.dirname(__file__), "../../../../../../")
            )

        return TemplateLocation(
            template_url=template_url,
            version="develop",
            sub_directory="templates/python",
        )

    def create_project_type_properties(
        self, project: Project
    ) -> typing.Mapping[str, typing.Any]:

        repo_url = project.hosting_service.get_repo_url(project)
        if project.project_details.project_url:
            project_url = project.project_details.project_url
        else:
            project_url = repo_url
        result = {
            "full_name": project.project_details.full_name,
            "email": project.project_details.email,
            "project_name": project.project_details.project_name,
            "project_slug": project.project_details.project_slug,
            "title": project.project_details.project_title,
            "exe_name": self.get_config("exe_name"),
            "project_main_module": self.get_config("project_main_module"),
            "project_short_description": project.project_details.project_short_description,
            "repo_url": repo_url,
            "project_url": project_url,
            "hosting_service": project.hosting_service.type_alias,
            "binary_download_base_url": "",
            "project_metadata_json": project.json(indent=2),
        }

        return result


class PrintPythonProjectMetadata(ProjectCommand):
    """Print project-type specific metadata for a Python project."""

    _cmd_name = "print-metadata"

    @classmethod
    def is_supported(cls, project: Project) -> bool:

        if project.project_type.type_alias == "python_project":
            return True
        else:
            return False

    def process(self, project: Project):

        md = PythonProjectMetadata(project.project_type.project_main_module)
        print(md.json(indent=2))
