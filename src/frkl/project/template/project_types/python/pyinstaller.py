# -*- coding: utf-8 -*-
import importlib
import jinja2
import json
import logging
import os
import tempfile
import typing
from datetime import datetime
from jinja2 import Environment
from pathlib import Path
from typing import (
    Any,
    Dict,
    Iterable,
    List,
    Mapping,
    MutableMapping,
    Optional,
    Set,
    Tuple,
)

from frkl.project.defaults import FRKL_PROJECT_RESOURCES_FOLDER
from frkl.project.meta.core import PythonProjectMetadata
from frkl.project.template import Project
from frkl.project.template.project_types.python import PythonProjectType
from frkl.project.template.project_types.python.utils import find_frkl_projects

try:
    from importlib_metadata import entry_points  # type: ignore
except Exception:
    from importlib.metadata import entry_points  # type: ignore

log = logging.getLogger("frkl.project")

DEFAULT_EXCLUDE_DIRS = [".git", ".tox", ".cache", ".mypy_cache"]


# -----------------------------------------------------------
# helper methods
def get_entry_point_imports_hook(
    entry_points: Mapping[str, Mapping[str, Mapping[str, str]]],
) -> Tuple[Mapping[str, str], Set]:
    """Return hooks and hiddenimport from the provided entry_points. """

    hook_ep_packages: Dict[str, List[str]] = dict()
    hiddenimports = set()

    for group, ep_details in entry_points.items():

        for ep_name, ep_dict in ep_details.items():

            module_name = ep_dict["module"]
            attr = ep_dict["attr"]
            hiddenimports.add(module_name)
            hook_ep_packages.setdefault(group, []).append(
                f"{ep_name} = {module_name}:{attr}"
            )

    hooks = {}
    hooks[
        "pkg_resources_hook.py"
    ] = """# Runtime hook generated from spec file to support pkg_resources entrypoints.
ep_packages = {}

if ep_packages:
    import pkg_resources
    default_iter_entry_points = pkg_resources.iter_entry_points

    def hook_iter_entry_points(group, name=None):
        if group in ep_packages and ep_packages[group]:
            eps = ep_packages[group]
            for ep in eps:
                parsedEp = pkg_resources.EntryPoint.parse(ep)
                parsedEp.dist = pkg_resources.Distribution()
                yield parsedEp
        else:
            yield default_iter_entry_points(group, name)
            return

    pkg_resources.iter_entry_points = hook_iter_entry_points

""".format(
        hook_ep_packages
    )

    log.debug(f"Retrieved hooks: {hooks}")
    log.debug(f"Retrieved hidden imports: {hiddenimports}")

    return hooks, hiddenimports


def process_string_template(
    template_string: str, replacement_dict: Optional[Mapping[str, Any]] = None
) -> str:
    """Replace template markers with values from a replacement dictionary within a string.

    Arguments:
      - *template_string*: the template string
      - *replacement_dict*: the dictionary with the replacement strings
      - *jinja_env*: an existing jinja env to use, if specified, the following paramters will be ignored

    Return:
        the template as a string
    """

    if replacement_dict is None:
        sub_dict = {}
    else:
        sub_dict = dict(replacement_dict)

    env = Environment()

    if isinstance(template_string, jinja2.runtime.Undefined):
        return ""

    # add some keywords, to make sure we don't get any weird internal result objects
    sub_dict.setdefault("namespace", None)

    result = env.from_string(template_string).render(sub_dict)

    if isinstance(result, jinja2.runtime.Undefined):
        result = ""

    return result


def get_datas(
    resources_map: Mapping[str, List[str]], exclude_dirs: Optional[Iterable[str]] = None
) -> typing.List[Tuple[str, str]]:
    """Get the 'datas' list of tuples for PyInstaller."""

    if exclude_dirs is None:
        exclude_dirs = DEFAULT_EXCLUDE_DIRS

    datas = []

    for package, files in resources_map.items():

        proot = os.path.dirname(importlib.import_module(package).__file__)
        for f in files:
            src = os.path.join(proot, f)
            if not os.path.isdir(os.path.realpath(src)):
                data = (src, package)
                datas.append(data)
            else:

                for root, dirnames, filenames in os.walk(src, topdown=True):

                    if exclude_dirs:
                        dirnames[:] = [d for d in dirnames if d not in exclude_dirs]

                    for filename in filenames:

                        path = os.path.join(root, filename)
                        rel_path = os.path.join(
                            package, os.path.dirname(os.path.relpath(path, proot))
                        )
                        data = (path, rel_path)
                        datas.append(data)

    log.debug(f"Retrieved pkg data: {datas}")
    return datas


def create_entry_point_from_template(
    main_module: str,
    working_dir: str,
    entry_points: Mapping[str, Mapping[str, Mapping[str, str]]],
):
    """Create entry_point code from a template."""

    if not entry_points:
        raise Exception("No entry_points provided.")

    console_scripts = {}

    main_entry_points: List[Mapping[str, Any]] = []

    for group, details in entry_points.items():

        if group != "console_scripts":
            continue

        for ep_name, ep_details in details.items():
            console_scripts[ep_name] = ep_details
            if main_module in ep_details["module"]:
                if main_entry_points:
                    log.warning(
                        f"Can't determine unique entry point for module: {main_module}, registering main application will be unpredictable"
                    )
                main_entry_points.append(ep_details)

    template = Path(
        os.path.join(FRKL_PROJECT_RESOURCES_FOLDER, "build", "entry_point.py.j2")
    )
    template_string = template.read_text()

    replaced = process_string_template(
        template_string=template_string,
        replacement_dict={
            "scripts": console_scripts,
            "main_entry_point": main_entry_points[0],
        },
    )

    target = Path(os.path.join(working_dir, "cli.py"))
    target.write_text(replaced)

    return [target.resolve().as_posix()]


def create_build_properties(project: PythonProjectMetadata) -> Mapping[str, Any]:

    pyinstaller_data = project.package_metadata.get("build_properties", {})

    result: Dict[str, Any] = {"entry_points": {}}

    for prop in ["resources", "hidden_imports"]:
        result[prop] = set(pyinstaller_data.get(prop, []))

    project_main_module = (
        f"{project.project_metadata['project']['project_main_module']}"
    )

    result["hidden_imports"].add(f"{project_main_module}.defaults")

    # finding entry points
    for entry_point_group, eps in entry_points().items():

        for ep in eps:
            if not ep.value.startswith(project_main_module):
                continue

            if hasattr(ep, "module"):
                module = ep.module
                attr = ep.attr
            else:
                module, attr = ep.value.split(":", maxsplit=1)

            result["entry_points"].setdefault(entry_point_group, {})[ep.name] = {
                "module": module,
                "attr": attr,
            }

    mod_path = project.module_path

    mod_parent = os.path.dirname(mod_path)
    resources_folder = os.path.join(mod_parent, "resources")
    if os.path.exists(resources_folder) and resources_folder not in result["resources"]:
        result["resources"].add(resources_folder)

    version_file = os.path.join(mod_parent, "version.txt")
    if os.path.exists(version_file) and version_file not in result["resources"]:
        result["resources"].add(version_file)
    frkl_file = os.path.join(mod_parent, "_frkl.json")
    if os.path.exists(frkl_file) and frkl_file not in result["resources"]:
        result["resources"].add(frkl_file)

    _meta_mod_path = os.path.join(mod_path, "_frkl")
    frkl_file = os.path.join(_meta_mod_path, "_frkl.json")
    if os.path.exists(frkl_file) and frkl_file not in result["resources"]:
        result["resources"].add(frkl_file)

    # runtime_hooks = os.path.join(os.path.dirname(_meta_mod_path), "pyinstaller_hooks")
    # if (
    #     os.path.isdir(runtime_hooks)
    #     and runtime_hooks not in result["hooks_path"]
    # ):
    #     result["hooks_path"].add(runtime_hooks)

    return result


class PyinstallerConfig(object):
    def __init__(self, project: Project):

        self._project: Project = project
        # TODO: assert we have the right project type
        self._project_data: PythonProjectMetadata = PythonProjectMetadata(
            project_main_module=project.project_type.get_config("project_main_module")
        )
        self._python_project_details: PythonProjectType = self._project.project_type

    @property
    def project(self) -> Project:
        return self._project

    @property
    def project_data(self) -> PythonProjectMetadata:
        return self._project_data

    @property
    def python_project_details(self) -> PythonProjectType:
        return self._python_project_details

    def create_package_data(self) -> Dict[str, Any]:

        build_properties = create_build_properties(self.project_data)

        # hooks_path: Set[str] = set()

        hidden_imports: Set[str] = build_properties["hidden_imports"]

        entry_points: Dict[str, MutableMapping[str, Mapping[str, str]]] = {}
        for group, details in build_properties["entry_points"].items():

            for ep_name, ep_details in details.items():
                if ep_name in entry_points.setdefault(group, {}).keys():
                    raise Exception(
                        f"Duplicate entry point name '{ep_name}' for group '{group}'."
                    )
                entry_points[group][ep_name] = ep_details

                hidden_imports.add(ep_details["module"])

        resources_map = {
            self.python_project_details.get_config(
                "project_main_module"
            ): build_properties["resources"]
        }

        # TODO: add resources from other projects
        other_projects: Mapping[str, PythonProjectMetadata] = find_frkl_projects(
            exclude_main_modules=[
                self.python_project_details.get_config("project_main_module")
            ]
        )
        for name, md in other_projects.items():
            other_build_properties = create_build_properties(md)
            resources_map[name] = other_build_properties["resources"]
            hidden_imports.update(other_build_properties["hidden_imports"])

            for group, details in other_build_properties["entry_points"].items():

                for ep_name, ep_details in details.items():
                    if ep_name in entry_points.setdefault(group, {}).keys():
                        if ep_details != entry_points[group][ep_name]:
                            raise Exception(
                                f"Duplicate entry point name '{ep_name}' for group '{group}'."
                            )
                        else:
                            continue
                    entry_points[group][ep_name] = ep_details
                    hidden_imports.add(ep_details["module"])

        # runtime_details = self.runtime_details

        app_details = self.project_data.dict()
        build_time = datetime.utcnow()
        app_details["build_info"] = {"build_time": str(build_time)}

        return {
            "app_details": app_details,
            "hidden_imports": hidden_imports,
            "resources": resources_map,
            "entry_points": entry_points,
        }

    def get_exe_name(self):

        return self._project_data.project_metadata["project_type"]["exe_name"]

    def get_main_module(self):

        return self._project_data.project_metadata["project_type"][
            "project_main_module"
        ]

    def create_analysis_args(self, path: typing.Union[str, Path] = None):

        if isinstance(path, Path):
            path = path.as_posix()

        package_data = self.create_package_data()
        app_details = package_data["app_details"]

        main_module = app_details["metadata"]["project"]["project_type"][
            "project_main_module"
        ]
        entry_points = package_data["entry_points"]

        hidden_imports = package_data["hidden_imports"]

        # hooks_path = None
        block_cipher = None

        if not path:
            working_dir = tempfile.mkdtemp(prefix="pkg_build")
        else:
            path = os.path.abspath(os.path.expanduser(path))
            os.makedirs(path, exist_ok=True)
            working_dir = path

        datas = get_datas(resources_map=package_data["resources"])

        # ep_hooks, auto_imports = get_entry_point_imports_hook(entry_points=entry_points)
        # runtime_hooks = []
        # for filename, hook_string in ep_hooks.items():
        #     path = os.path.join(tempdir, filename)
        #     with io.open(path, "w", encoding="utf-8") as f:
        #         f.write(hook_string)
        #     runtime_hooks.append(path)

        sc = create_entry_point_from_template(
            main_module=main_module, working_dir=working_dir, entry_points=entry_points
        )

        app_details_file = os.path.join(working_dir, "app.json")
        with open(app_details_file, "w") as f:  # type: ignore
            json.dump(app_details, f)
        d = (app_details_file, main_module)
        datas.append(d)

        kwargs = dict(
            scripts=sc,
            pathex=[],
            binaries=[],
            datas=datas,
            hiddenimports=list(hidden_imports),
            # hookspath=hooks_path,
            # runtime_hooks=runtime_hooks,
            runtime_hooks=None,
            excludes=["FixTk", "tcl", "tk", "_tkinter", "tkinter", "Tkinter"],
            win_no_prefer_redirects=False,
            win_private_assemblies=False,
            cipher=block_cipher,
            noarchive=False,
        )

        log.debug(f"Created analysis args: {kwargs}")

        return kwargs
