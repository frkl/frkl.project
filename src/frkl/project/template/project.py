# -*- coding: utf-8 -*-
import typing
from pydantic import BaseModel, EmailStr, Field, constr, root_validator, validator
from slugify import slugify

from frkl.common.exceptions import FrklException
from frkl.project.template.hosting_services import HostingService
from frkl.project.template.project_types import ProjectCommand
from frkl.typistry import Tyng, Typistry

if typing.TYPE_CHECKING:
    from frkl.project.template import ProjectType


class ProjectDetails(BaseModel):

    full_name: str = Field(description="The author name.")
    email: EmailStr = Field(description="The author email.")
    project_name: constr(min_length=1) = Field(description="The project name (can't contain whitespace).")  # type: ignore
    project_slug: str = Field(
        description="The project slug.",
        default_desc="Slugified 'project_name' value (can't contain whitespace nor punctuation).",
    )
    project_title: str = Field(
        description="The project title.", default_desc="The project title."
    )
    project_short_description: str = Field(
        description="The project description.", default="-- n/a --"
    )
    project_url: typing.Optional[str] = Field(
        description="The project homepage.",
        default=None,
        default_desc="The project repo url.",
    )

    @root_validator(pre=True)
    def _fill_defaults(cls, values):

        if not values.get("project_slug", None):
            values["project_slug"] = slugify(values["project_name"], separator="_")
        if not values.get("project_title", None):
            values["project_title"] = values["project_name"]

        return values

    @validator("project_slug")
    def _validate_default_project_slug(cls, v, *, values, **kwargs):

        assert "." not in v
        assert "-" not in v
        assert "\t" not in v
        assert "\n" not in v
        assert " " not in v

        # TODO: make this a regex, this needs a lot more characters to test for

        return v

    @validator("project_name", pre=True)
    def _validate_project_name(cls, v):

        assert isinstance(v, str)
        assert " " not in v
        assert "\t" not in v
        assert "\n" not in v

        return v


class Project(Tyng):
    def __init__(self, typistry: Typistry, **kwargs):

        self._typistry: Typistry = typistry
        self._commands: typing.Optional[typing.Dict[str, ProjectCommand]] = None
        super().__init__(**kwargs)

    @property
    def hosting_service(self) -> HostingService:
        return self.get_config("hosting_service")

    @hosting_service.setter
    def hosting_service(
        self,
        hosting_service: typing.Union[typing.Mapping[str, typing.Any], HostingService],
    ):
        self.set_config("hosting_service", hosting_service)

    @property
    def project_type(self) -> "ProjectType":
        return self.get_config("project_type")

    @project_type.setter
    def project_type(
        self,
        project_type: typing.Union[typing.Mapping[str, typing.Any], "ProjectType"],
    ):
        self.set_config("project_type", project_type)

    @property
    def project_details(self) -> ProjectDetails:
        return self.get_config("project_details")

    @property
    def commands(self) -> typing.Dict[str, ProjectCommand]:

        if self._commands is not None:
            return self._commands

        commands = self._typistry.get_tyng_subclasses(ProjectCommand)

        self._commands = {}
        for c_name, c in commands.items():
            if c.is_supported(self):
                self._commands[c_name] = c

        return self._commands

    def execute_command(self, command, **kwargs):

        command = self.commands.get(command, None)
        if command is None:
            raise FrklException(
                f"Can't execute command '{command}' for project '{self.project_details.project_name}'.",
                reason="Command not available (for this project type, anyway).",
            )

        c: ProjectCommand = self._typistry.create_tyng_obj(command, kwargs)
        result = c.process(self)

        return result

    def create_project_on_hosting_service(self):

        pass
