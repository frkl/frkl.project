# -*- coding: utf-8 -*-

"""Module to hold all metadata-related classes and utilities."""

import copy
import importlib
import json
import logging
import os
import sys
import types
import typing
from appdirs import AppDirs
from pathlib import Path
from types import ModuleType
from typing import Any, Callable, Coroutine, Dict, Mapping, Optional, Union

from frkl.project.defaults import DEFAULT_PACKAGE_METADATA_MODULE_NAME

log = logging.getLogger("frkl")


def retrieve_metadata_for_project_main_module(
    project_main_module: Union[str, types.ModuleType]
) -> typing.Mapping[str, Any]:

    if isinstance(project_main_module, str):
        project_main_module = importlib.import_module(project_main_module)

    module_name = project_main_module.__name__

    rel_path_to_root = [".." for _ in module_name.split(".")] + [".."]
    root = os.path.abspath(
        os.path.join(project_main_module.__file__, *rel_path_to_root)
    )
    project_metadata_file = Path(os.path.join(root, f".{module_name}.project"))

    if not project_metadata_file.exists():
        raise Exception(
            f"Can't retrieve project metadata for project with main module '{module_name}': no metadata file available at '{project_metadata_file.as_posix()}'"
        )

    project_metadata_content = project_metadata_file.read_text()

    try:
        project_metadata = json.loads(project_metadata_content)
    except Exception as e:
        raise Exception(
            f"Can't retrieve project metadata for project with main module '{module_name}': error parsing metadata file ({e})"
        )

    return project_metadata


def find_project_metadata(
    project_main_module: Union[str, types.ModuleType]
) -> Mapping[str, Any]:

    if isinstance(project_main_module, str):

        try:
            project_main_module = importlib.import_module(project_main_module)
        except (Exception):
            # log.debug(
            #     f"Pkg '{self.main_module}' does not have a '_meta' module, returning empty dict..."
            # )
            raise Exception(
                f"Can't retrieve project metadata for project with main module '{project_main_module}': module not available."
            )

    if not hasattr(sys, "frozen"):
        project_metadata = retrieve_metadata_for_project_main_module(
            project_main_module
        )
    else:
        raise NotImplementedError()

    return project_metadata


def retrieve_package_metadata(project_main_module: str) -> Mapping[str, Any]:

    try:
        metadata_module = importlib.import_module(
            f"{project_main_module}.{DEFAULT_PACKAGE_METADATA_MODULE_NAME}"
        )
    except (Exception):
        log.debug(
            f"Pkg '{project_main_module}' does not have a '{DEFAULT_PACKAGE_METADATA_MODULE_NAME}' module, returning empty dict..."
        )
        return {}

    result = {}
    for k in dir(metadata_module):

        if k.startswith("_"):
            continue

        attr = getattr(metadata_module, k)
        if isinstance(
            attr, (ModuleType, type, Callable, Coroutine)  # type: ignore
        ) or str(attr).startswith("typing."):
            continue

        result[k] = attr

    return result


class PythonProjectMetadata(object):
    """Class to hold all relevant information of a frkl-Python package"""

    def __init__(self, project_main_module: Union[str, types.ModuleType]):

        if isinstance(project_main_module, types.ModuleType):
            project_main_module = project_main_module.__name__

        self._project_main_module: str = project_main_module  # type: ignore
        """The main module for this application"""

        self._runtime_details: Optional[Mapping[str, Any]] = None
        """Details about the current app (whether it's a binary, build-time, etc)."""

        self._build_info: Optional[Mapping[str, Any]] = None
        """Details about the build (if running as pyinstaller binary)."""

        self._project_metadata: Optional[Mapping[str, Mapping[str, Any]]] = None
        """Project metadata for this application."""

        self._package_metadata: Optional[Mapping[str, Mapping[str, Any]]] = None
        """Metadata related to the python package that gets created with this project."""

        self._version: Optional[str] = None
        """The version of the package."""

        self._other_metadata_projects: Optional[
            Mapping[str, PythonProjectMetadata]
        ] = None
        """A dictionary to hold information about the main dependency packages/modules of this applicaiton."""

        self._other_metadata_project_versions: Optional[Mapping[str, str]] = None
        """A dictionary to hold version information of main dependency packages of this application."""

        self._package_defaults: Optional[Mapping[str, Any]] = None
        """Default values for this package (atribures in the '<main_module>.defaults' module)."""

        # if this is distributed as a frozen bundle, we read metadata from a file
        # otherwise it will be read dynamically
        self._check_app_metadata_file()

        # for k, v in globals.items():
        #     self.set_global(k, v)

    @property
    def main_module(self) -> str:
        """Return the name of the main module of this project."""
        return self._project_main_module

    def _check_app_metadata_file(self) -> None:

        # we only use that if we are dealing with a pyinstaller binary
        if not hasattr(sys, "frozen"):
            return None

        app_details_file = os.path.join(
            sys._MEIPASS, self.main_module, "app.json"  # type: ignore
        )
        if os.path.exists(app_details_file):
            log.debug(f"'app.json' file exists: {app_details_file}")
            with open(app_details_file, "r") as f:
                app_details = json.load(f)
        else:
            raise Exception(f"No 'app.json' file: {app_details_file}")

        self._project_metadata = app_details["metadata"]
        self._version = app_details["version"]
        # self._other_metadata_projects = app_details["modules_details"]
        self._build_info = app_details["build_info"]

    @property
    def project_metadata(self) -> Mapping[str, Any]:
        """Return metadata of this package."""

        if self._project_metadata is not None:
            return self._project_metadata

        if not self.main_module:
            raise Exception(
                "Can't retrieve app details: AppEnvironment not initialized yet."
            )

        self._project_metadata = find_project_metadata(self.main_module)
        return self._project_metadata

    @property
    def package_metadata(self) -> typing.Mapping[str, Any]:

        if self._package_metadata is None:
            self._package_metadata = retrieve_package_metadata(self.main_module)
        return self._package_metadata

    @property
    def project_name(self) -> str:
        """Return the project name."""
        return self.project_metadata["project_details"]["project_name"]

    @property
    def project_slug(self) -> str:
        """Return the project slug."""
        return self.project_metadata["project_details"]["project_slug"]

    @property
    def runtime_details(self) -> Mapping[str, Any]:
        """Method to get information about the running package.

        This is mainly concerned about application artefact metadata, like build time, etc.
        """

        if self._runtime_details is not None:
            return self._runtime_details

        app_details: Dict[str, Any] = {}
        # finding console_scripts entry point
        # entry_point = None
        # for entry_point_group, eps in entry_points().items():
        #
        #     if entry_point_group != "console_scripts":
        #         continue
        #
        #     for ep in eps:
        #         if ep.name != self.exe_name:
        #             continue
        #         entry_point = {"name": ep.name, "module": ep.module, "attr": ep.attr}
        #
        #
        # app_details["entry_point"] = entry_point
        if not hasattr(sys, "frozen"):
            log.debug("creating app details (not frozen).")

            app_details["app_type"] = "python-env"

            app_details["build_info"] = {}

        else:
            app_details["app_type"] = "binary"
            app_details["build_info"] = self._build_info

        self._runtime_details = app_details
        return self._runtime_details

    @property
    def version(self) -> str:

        if self._version is not None:
            return self._version

        m = importlib.import_module(self.main_module)
        try:
            version = getattr(m, "get_version")()
        except (Exception) as e:
            log.debug(f"Can't add version for pkg '{self.main_module}': {e}")
            version = "n/a"

        self._version = version
        return self._version

    def get_pkg_defaults(self) -> Mapping[str, Any]:

        if self._package_defaults is not None:
            return self._package_defaults

        try:
            defaults = importlib.import_module(".".join([self.main_module, "defaults"]))
        except (Exception) as e:
            log.warning(f"Can't retrieve defaults module for '{self.main_module}': {e}")
            return {}

        result = {}
        for k in dir(defaults):
            if k.startswith("_"):
                continue

            attr = getattr(defaults, k)
            if isinstance(attr, (ModuleType, type)) or str(attr).startswith("typing."):
                continue

            result[k] = attr

        self._package_defaults = result

        return self._package_defaults

    def get_app_dirs(self) -> Optional[AppDirs]:
        """Return 'AppDirs' object for this application."""

        for k, v in self.get_pkg_defaults().items():
            if isinstance(v, AppDirs):
                return v

        return None

    def get_resources_folder(self) -> str:
        """Return the resources folder path for this application."""

        # if "resources" in self.build_meta.keys():
        #     return self.build_meta["resources"]

        for k in self.get_pkg_defaults().keys():
            if k.endswith("RESOURCES_FOLDER"):
                return self.get_pkg_defaults()[k]

        raise Exception(f"Can't determine resources folder for '{self.project_name}'.")

    @property
    def module_path(self):

        m = importlib.import_module(self.main_module)
        return m.__file__

    def __repr__(self):

        return f"PythonProjectMetadata(main_module='{self.main_module}')"

    def dict(self) -> Dict[str, Any]:
        """Export project metadata as a dict."""

        result: Dict[str, Any] = {}
        result["project_metadata"] = copy.deepcopy(self.project_metadata)
        result["package_metadata"] = copy.deepcopy(self.package_metadata)
        result["runtime_details"] = copy.deepcopy(self.runtime_details)
        result["version"] = self.version

        return result

    def json(self, **kwargs) -> str:

        return json.dumps(self.dict(), **kwargs)
