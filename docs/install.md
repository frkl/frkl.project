# Installation

There are three ways to install *frkl.project* on your machine. Via a manual binary download, an install script, or installation of the python package.

## Binaries

To install the `frkl-project` binary, download the appropriate binary from one of the links below, and set the downloaded file to be executable (``chmod +x frkl-project``):

  - [Linux](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/frkl-project)
  - [Windows](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/windows/frkl-project.exe)
  - [Mac OS X](https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/darwin/frkl-project)


## Install script

Alternatively, use the 'curly' install script for `frkl-project`:

``` console
curl https://gitlab.com/frkl/frkl.project/-/raw/develop/scripts/install/frkl-project.sh | bash
```


This will add a section to your shell init file to add the install location (``$HOME/.local/share/frkl/bin``) to your ``$PATH``.

You might need to source that file (or log out and re-log in to your session) in order to be able to use *frkl-project*:

``` console
source ~/.profile
```


## Python package

The python package is currently not available on [pypi](https://pypi.org), so you need to specify the ``--extra-url`` parameter for your pip command. If you chooose this install method, I assume you know how to install Python packages manually, which is why I only show you an example way of getting *frkl.project* onto your machine:

``` console
> python3 -m venv ~/.venvs/frkl.project
> source ~/.venvs/frkl.project/bin/activate
> pip install --extra-index-url https://gitlab.com/api/v4/projects/25344049/packages/pypi/simple frkl.project
...
...
Successfully installed aiokafka-0.6.0 aiopg-1.0.0 ... ... ...
> frkl.project --help
Usage: frkl.project [OPTIONS] COMMAND [ARGS]...
   ...
   ...
```
