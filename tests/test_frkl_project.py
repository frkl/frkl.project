#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `frkl_project` package."""

import pytest  # noqa

import frkl.project


def test_assert():

    assert frkl.project.get_version() is not None
