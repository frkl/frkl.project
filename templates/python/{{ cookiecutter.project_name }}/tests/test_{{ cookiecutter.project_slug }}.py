#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `{{ cookiecutter.project_slug }}` package."""

import {{ cookiecutter.project_main_module }}
import pytest  # noqa


def test_assert():

    assert {{ cookiecutter.project_main_module }}.get_version() is not None
