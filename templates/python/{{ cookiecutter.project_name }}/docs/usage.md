# Usage

{% if cookiecutter.exe_name %}
## Getting help

To get information for the `{{ cookiecutter.exe_name }}` command, use the ``--help`` flag:

{{ "{{" }} cli("{{ cookiecutter.exe_name }}", "--help") {{ "}}" }}
{% else %}
TO BE DONE
{% endif %}
