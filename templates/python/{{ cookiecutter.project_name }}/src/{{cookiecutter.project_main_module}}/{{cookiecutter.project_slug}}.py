# -*- coding: utf-8 -*-

"""Main module of {{ cookiecutter.project_name }}."""

import logging
log = logging.getLogger("{{ cookiecutter.project_slug }}")
