# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


{{ cookiecutter.project_slug }}_app_dirs = AppDirs("{{ cookiecutter.project_slug }}", "frkl")

if not hasattr(sys, "frozen"):
    {{ cookiecutter.project_slug | upper }}_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `{{ cookiecutter.project_slug }}` module."""
else:
    {{ cookiecutter.project_slug | upper }}_MODULE_BASE_FOLDER = os.path.join(
        sys._MEIPASS, "{{ cookiecutter.project_slug }}"  # type: ignore
    )
    """Marker to indicate the base folder for the `{{ cookiecutter.project_slug }}` module."""

{{ cookiecutter.project_slug | upper }}_RESOURCES_FOLDER = os.path.join(
    {{ cookiecutter.project_slug | upper }}_MODULE_BASE_FOLDER, "resources"
)
"""Default resources folder for this package."""
