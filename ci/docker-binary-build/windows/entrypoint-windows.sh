#!/bin/bash

# Fail on errors.
set -e

# Make sure .bashrc is sourced
source /root/.bashrc

# Allow the workdir to be set using an env var.
# Useful for CI pipelines which use docker for their build steps
# and don't allow that much flexibility to mount volumes
export WORKDIR=${SRCDIR:-/src}
export WORKDIR_IN_WINE="$(winepath -w ${WORKDIR})"
#export WINEPATH=$(winepath -w /opt/git/cmd)

# In case the user specified a custom URL for PYPI, then use
# that one, instead of the default one.
#
if [[ "$PYPI_URL" != "https://pypi.python.org/" ]] || \
   [[ "$PYPI_INDEX_URL" != "https://pypi.python.org/simple" ]]; then
    # the funky looking regexp just extracts the hostname, excluding port
    # to be used as a trusted-host.
    mkdir -p /wine/drive_c/users/root/pip
    echo "[global]" > /wine/drive_c/users/root/pip/pip.ini
    echo "index = $PYPI_URL" >> /wine/drive_c/users/root/pip/pip.ini
    echo "index-url = $PYPI_INDEX_URL" >> /wine/drive_c/users/root/pip/pip.ini
    echo "trusted-host = $(echo $PYPI_URL | perl -pe 's|^.*?://(.*?)(:.*?)?/.*$|$1|')" >> /wine/drive_c/users/root/pip/pip.ini

    echo "Using custom pip.ini: "
    cat /wine/drive_c/users/root/pip/pip.ini
fi

export GIT_PYTHON_REFRESH=quiet

cd $WORKDIR

#/usr/bin/pip install -U --extra-index-url https://pkgs.frkl.io/frkl/dev --extra-index-url https://pkgs.frkl.dev/pypi "${WORKDIR_IN_WINE}[all,dev_build]"

WHEEL_NAME="$(ls dist/*.whl)"
/usr/bin/pip install -U --extra-index-url https://gitlab.com/api/v4/projects/25344049/packages/pypi/simple "${WORKDIR_IN_WINE}/${WHEEL_NAME}[all,dev_build]"

if [[ "$@" == "" ]]; then
    wine /wine/drive_c/Python/Scripts/frkl-project.exe metadata print --output '.frkl\\project.json'
    wine /wine/drive_c/Python/Scripts/frkl-project.exe metadata print-pyinstaller-data --output '.frkl\\pyinstaller'
    chown -R --reference=. ".frkl"
    pyinstaller --clean -y --dist 'dist\\windows' --workpath /tmp 'scripts\\build-binary\\onefile.spec'
    chown -R --reference=. ./dist/windows
else
    sh -c "$@"
fi # [[ "$@" == "" ]]
