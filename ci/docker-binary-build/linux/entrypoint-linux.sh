#!/bin/bash -i

# Fail on errors.
set -e

# Make sure .bashrc is sourced
source "${HOME}/.bashrc"

# Allow the workdir to be set using an env var.
# Useful for CI pipiles which use docker for their build steps
# and don't allow that much flexibility to mount volumes
WORKDIR=${SRCDIR:-/src}
TEMPDIR=${TMPDIR:-/tmp}

if [[ -z "$PYPI_URL" ]]; then
  PYPI_URL="https://pypi.python.org"
fi

if [[ -z "$PYPI_INDEX_URL" ]]; then
  PYPI_INDEX_URL="https://pypi.python.org/simple"
fi

# In case the user specified a custom URL for PYPI, then use
# that one, instead of the default one.
#
if [[ "$PYPI_URL" != "https://pypi.python.org/" ]] || [[ "$PYPI_INDEX_URL" != "https://pypi.python.org/simple" ]]; then
    mkdir -p "${HOME}/.pip"
    echo "[global]" > "${HOME}/.pip/pip.conf"
    echo "index = $PYPI_URL" >> "${HOME}/.pip/pip.conf"
    echo "index-url = $PYPI_INDEX_URL" >> "${HOME}/.pip/pip.conf"
    echo "trusted-host = $(echo $PYPI_URL | perl -pe 's|^.*?://(.*?)(:.*?)?/.*$|$1|')" >> "${HOME}/.pip/pip.conf"

    echo "Using custom pip.conf:"
    cat "${HOME}/.pip/pip.conf"
fi

cd ${WORKDIR}

# shellcheck disable=SC2199
if [[ "$@" == "" ]]; then
    /opt/build.sh
else
    sh -c "$@"
fi
